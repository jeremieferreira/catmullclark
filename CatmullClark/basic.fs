#version 330

in vec3 g_normal;
in vec4 g_position;

out vec4 o_color;

void main(void) {
	vec3 lightPos = vec3(1.0, 1.0, 2.0);
	vec3 L = normalize(lightPos - g_position.xyz);
	vec3 N = normalize(g_normal.xyz);

	float diffuse = max(dot(N, L), 0.0) * .5 + .3;
	o_color = vec4(vec3(diffuse), 1.0);
}