#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec4 v_position[3];

out vec3 g_normal;
out vec4 g_position;
out vec4 Fragment;

void main(void) {
	vec3 normal = normalize(cross(normalize(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz), normalize(gl_in[2].gl_Position.xyz - gl_in[1].gl_Position.xyz)));
	for (int i = 0; i < 3; ++i) {
		g_normal = normal;
		g_position = v_position[i];
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}
}