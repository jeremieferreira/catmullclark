#version 330

layout(triangles) in;
layout(line_strip, max_vertices=4) out;

out vec4 Fragment;

void main(void) {
	for (int i = 0; i < 3; ++i) {
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}

	gl_Position = gl_in[0].gl_Position;
	EmitVertex();
}