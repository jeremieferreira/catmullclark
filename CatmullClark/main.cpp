#define GLEW_STATIC
#include <gl/glew.h>
#include <gl/wglew.h>
#define FREEGLUT_LIB_PRAGMAS 0
#include <gl/freeglut.h>

#include <iostream>
#include <cstdint>
#include <cstdio>
#include <ctime>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "../commons/glShader.h"
#include "../commons/cube.h"

#include "Mesh.h"

struct Camera {
	glm::vec3 rotation;
	float radius;
	GLuint viewProjUBO;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
} g_Camera;

int g_PreviousTime;

GLShader g_BasicShader;
GLShader g_LineShader;

SceneObject* g_CubeIterations;

int g_MaxIterations;
int g_Iterations;
bool g_Color = true;
bool g_Wireframe = true;
bool g_Rotation = true;

void CreateCube(SceneObject& object) {
	object.AddVertex(glm::vec3(-1.f, -1.f, 1.0f));
	object.AddVertex(glm::vec3(1.f, -1.f, 1.0f));
	object.AddVertex(glm::vec3(1.f, 1.f, 1.0f));
	object.AddVertex(glm::vec3(-1.f, 1.f, 1.0f));
	object.AddVertex(glm::vec3(-1.f, -1.f, -1.0f));
	object.AddVertex(glm::vec3(1.f, -1.f, -1.0f));
	object.AddVertex(glm::vec3(1.f, 1.f, -1.0f));
	object.AddVertex(glm::vec3(-1.f, 1.f, -1.0f));

	// Faces
	object.AddFace(0, 1, 2, 3);		// avant
	object.AddFace(3, 2, 6, 7);		// haut
	object.AddFace(7, 6, 5, 4);		// arriere
	object.AddFace(1, 5, 6, 2);		// droite
	object.AddFace(4, 0, 3, 7);		// gauche
	object.AddFace(4, 5, 1, 0);		// bas

	// Compute mesh
	object.ComputeMesh();
}

void ShowHelp() {
	std::cout << "\n";
	std::cout << "=== Catmull Clark Subdivision ===\n";
	std::cout << "\n";
	std::cout << "= Usage =\n";
	std::cout << "CatmullClark.exe [iterations] [filepath]\n";
	std::cout << "  iterations: number of iterations to be computed\n";
	std::cout << "  filepath: to the obj file (only vertices and faces needed)\n";
	std::cout << "\n";
	std::cout << "= Navigation =\n";
	std::cout << "Subdivide more: Up Arrow\n";
	std::cout << "Subdivide less: Down Arrow\n";
	std::cout << "Toggle Rotation: R key\n";
	std::cout << "Toggle Wireframe: W key\n";
	std::cout << "Toggle Color: C key\n";
	std::cout << "Zoom in: Z key\n";
	std::cout << "Zoom out: S key\n";
	std::cout << "\n\n\n";
}

bool Initialize(int argc, char ** argv) {
	ShowHelp();

	if (argc > 2) {
		g_MaxIterations = atoi(argv[1]);
		g_CubeIterations = new SceneObject[g_MaxIterations];
		std::cout << g_MaxIterations << " iterations to compute\n";
		std::cout << "Loading " << argv[2] << "\n";
		g_CubeIterations[0].LoadObj(argv[2]);
	}
	else {
		g_MaxIterations = 2;
		g_CubeIterations = new SceneObject[g_MaxIterations];
		std::cout << "Loading object.obj\n";
		g_CubeIterations[0].LoadObj("object.obj");
	}

	GLenum status = glewInit();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	wglSwapIntervalEXT(1);

	// Compute MAX_ITERATIONS iterations of catmull-clark subdivision
	for (int i = 1; i < g_MaxIterations; ++i) {
		std::clock_t start = std::clock();
		std::cout << "Computing iteration #" << i;
		g_CubeIterations[i - 1].CatmullClark(g_CubeIterations[i]);
		double duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
		std::cout << " (" << duration << "s)\n";
	}

	glGenBuffers(1, &g_Camera.viewProjUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, g_Camera.viewProjUBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 2, NULL, GL_STREAM_DRAW);

	g_CubeIterations[0].BindObject();

	g_BasicShader.LoadVertexShader("basic.vs");
	g_BasicShader.LoadGeometryShader("basic.gs");
	g_BasicShader.LoadFragmentShader("basic.fs");
	g_BasicShader.Create();

	g_LineShader.LoadVertexShader("basic.vs");
	g_LineShader.LoadGeometryShader("line.gs");
	g_LineShader.LoadFragmentShader("line.fs");
	g_LineShader.Create();

	auto program = g_BasicShader.GetProgram();
	auto viewProjLocation = glGetUniformLocation(program, "ViewProj");
	glUniformBlockBinding(program, viewProjLocation, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, g_Camera.viewProjUBO);
	
	program = g_LineShader.GetProgram();
	viewProjLocation = glGetUniformLocation(program, "ViewProj");
	glUniformBlockBinding(program, viewProjLocation, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, g_Camera.viewProjUBO);

	g_Camera.radius = 4.f;

	return true;
}

void Terminate() {
	g_BasicShader.Destroy();
	g_LineShader.Destroy();
	delete[] g_CubeIterations;
}

void Resize(GLint width, GLint height) {
	glViewport(0, 0, width, height);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
	g_Camera.projectionMatrix = glm::perspectiveFov(45.f, (float)width, (float)height, 0.1f, 1000.f);
}

void Update() {
	int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
	int deltaTime = timeSinceStart - g_PreviousTime;
	g_PreviousTime = timeSinceStart;
	if (g_Rotation) {
		g_Camera.rotation += .03f * deltaTime;
	}
	glutPostRedisplay();
}

void Render() {
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	float rotY = glm::radians(g_Camera.rotation.y);
	glm::vec4 position = glm::vec4(0.f, 0.f, g_Camera.radius, 1.f);
	g_Camera.viewMatrix = glm::lookAt(glm::vec3(position), glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
	g_CubeIterations[g_Iterations].m_WorldMatrix = glm::eulerAngleY(rotY);

	glBindBuffer(GL_UNIFORM_BUFFER, g_Camera.viewProjUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4) * 2, glm::value_ptr(g_Camera.viewMatrix));

	if (g_Color) {
		auto basicProgram = g_BasicShader.GetProgram();
		glUseProgram(basicProgram);
		g_CubeIterations[g_Iterations].Draw(basicProgram);
	}

	if (g_Wireframe) {
		auto lineProgram = g_LineShader.GetProgram();
		glUseProgram(lineProgram);
		g_CubeIterations[g_Iterations].Draw(lineProgram);
	}
	
	glutSwapBuffers();
}

void Keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 'c':
	case 'C':
		g_Color = !g_Color;
		break;
	case 'z':
	case 'Z':
		g_Camera.radius -= .1f;
		break;
	case 's':
	case 'S':
		g_Camera.radius += .1f;
		break;
	case 'w':
	case 'W':
		g_Wireframe = !g_Wireframe;
		break;
	case 'r':
	case 'R':
		g_Rotation = !g_Rotation;
	}
}

void Special(int key, int x, int y) {
	if (key == GLUT_KEY_UP) {
		++g_Iterations;
		if (g_Iterations >= g_MaxIterations - 1) g_Iterations = g_MaxIterations - 1;
		std::cout << g_Iterations << " iterations\n";
	} else if (key == GLUT_KEY_DOWN) {
		--g_Iterations;
		if (g_Iterations < 0) g_Iterations = 0;
		std::cout << g_Iterations << " iterations\n";
	}

	g_CubeIterations[g_Iterations].BindObject();
}

int main(int argc, char ** argv) {
	glutInit(&argc, argv);
	glutInitWindowSize(1280, 720);
	glutCreateWindow("Catmull Clark Subdivision");
	Initialize(argc, argv);
	glutIdleFunc(Update);
	glutDisplayFunc(Render);
	glutReshapeFunc(Resize);
	glutSpecialFunc(Special);
	glutKeyboardFunc(Keyboard);
#if FREEGLUT
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
#endif
	glutMainLoop();
	Terminate();
	return 1;
}