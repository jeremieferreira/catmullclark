#pragma once

#include <cstdint>
#include <vector>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>

#define GLEW_STATIC
#include <gl/glew.h>
#include <gl/wglew.h>

struct subVertex {
	uint16_t index;
	glm::vec3 position;
	subVertex(uint16_t i, glm::vec3 p) : index(i), position(p) {}
};

struct vert {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 UV;
};

class Vertex;
class QuadMesh;
class Face;

class Edge {
public:
	uint16_t m_Index;
	Vertex* m_V1;
	Vertex* m_V2;
	Face* m_F1;
	Face* m_F2;
	subVertex* m_EdgePoint;	// CatmullClark algorithm edge point
};

class Face {
public:
	uint16_t m_Index;
	glm::vec3 m_Normal;
	subVertex* m_FacePoint;	// CatmullClark algorithm face point
	std::vector<Vertex*> m_Vertices;
	std::vector<Edge*> m_Edges;
};

class Vertex {
public:
	uint16_t m_Index;
	glm::vec3 m_Position;
	glm::vec3 m_Normal;
	subVertex* m_VertexPoint;	// CatmullClark algorithm vertex point
	std::vector<Edge*> m_Edges;
	std::vector<Face*> m_Faces;
};

class SceneObject {
public:
	// modeling stuff
	uint16_t m_Index;
	std::vector<Vertex*> m_Vertices;
	std::vector<Face*> m_Faces;
	std::vector<Edge*> m_Edges;

	// opengl stuff
	uint32_t VBO, IBO, VAO;
	std::vector<vert> m_MeshVertices;
	std::vector<float> m_MeshFloatVertices;
	std::vector<uint16_t> m_MeshIndices;
	glm::mat4 m_WorldMatrix;

	// methods
	bool LoadObj(const char* filepath);
	void CatmullClark(SceneObject& subdividedObject) const;
	void AddVertex(glm::vec3 position);
	void AddFace(uint16_t v1, uint16_t v2, uint16_t v3);
	void AddFace(uint16_t v1, uint16_t v2, uint16_t v3, uint16_t v4);
	void ComputeMesh();

	void BindObject();
	void Draw(uint32_t program);

	~SceneObject();

private:
	Edge* FindEdge(uint16_t v1, uint16_t v2) const;

	bool FindEdgesFromVerticeAndFace(const Face* face, const Vertex* vertex, Edge** e1, Edge** e2) const;

	/**
	 * Add an edge if it does not already exist. The last face in the m_Faces vector
	 * is set as first or second edge face according to the previous existance of
	 * this edge.
	 */
	void AddEdge(uint16_t v1, uint16_t v2);
};