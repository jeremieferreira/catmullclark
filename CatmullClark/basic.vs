#version 330

layout(location=0) in vec4 a_position;

layout(std140) uniform ViewProj {
	mat4 u_viewMatrix;
	mat4 u_projectionMatrix;
};

uniform mat4 u_worldMatrix;

out vec4 v_position;

void main(void) {
	v_position = a_position;
	gl_Position = u_projectionMatrix * u_viewMatrix * u_worldMatrix * a_position;
}
