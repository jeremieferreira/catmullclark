#include "Mesh.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

bool SceneObject::LoadObj(const char* filepath) {
	std::fstream file;
	file.open(filepath);

	if (!file) return false;

	std::string line;
	while (std::getline(file, line)) {

		if (line[0] == 'v') {
			std::istringstream ss(line);
			glm::vec3 pos;
			std::string s;
			ss >> s >> pos.x >> pos.y >> pos.z;
			this->AddVertex(pos);
			ss.clear();
		}
		else if (line[0] == 'f') {
			std::istringstream ss(line);
			uint16_t v1, v2, v3, v4 = 65535;
			std::string s;
			ss >> s >> v1 >> v2 >> v3 >> v4;
			if (v4 == 65535) {
				this->AddFace(v1 - 1, v2 - 1, v3 - 1);
			}
			else {
				this->AddFace(v1 - 1, v2 - 1, v3 - 1, v4 - 1);
			}
			ss.clear();
		}
	}

	this->ComputeMesh();
}

Edge* SceneObject::FindEdge(uint16_t v1, uint16_t v2) const {
	
	for (auto i = 0; i < this->m_Edges.size(); ++i) {
		Edge* e = this->m_Edges[i];
		if (e->m_V1->m_Index == v1 && e->m_V2->m_Index == v2 || e->m_V1->m_Index == v2 && e->m_V2->m_Index == v1) {
			return e;
		}
	}

	return NULL;
}

void SceneObject::AddEdge(uint16_t v1, uint16_t v2) {
	Face* lastFace = this->m_Faces.back();
	Edge* edge = this->FindEdge(v1, v2);
	if (edge != NULL) {
		edge->m_F2 = lastFace;
	}
	else {
		edge = new Edge;
		edge->m_Index = m_Edges.size();
		edge->m_V1 = this->m_Vertices[v1];
		edge->m_V2 = this->m_Vertices[v2];
		edge->m_F1 = lastFace;
		edge->m_F2 = NULL;
		edge->m_V1->m_Edges.emplace_back(edge);
		edge->m_V2->m_Edges.emplace_back(edge);
		this->m_Edges.emplace_back(edge);
	}
	lastFace->m_Edges.emplace_back(edge);
}

void SceneObject::AddVertex(glm::vec3 position) {
	Vertex* vertex = new Vertex;
	vertex->m_Index = this->m_Vertices.size();
	vertex->m_Position = position;
	this->m_Vertices.emplace_back(vertex);
}

void SceneObject::AddFace(uint16_t v1, uint16_t v2, uint16_t v3) {
	Face* face = new Face;

	face->m_Index = m_Faces.size();
	face->m_Vertices.emplace_back(this->m_Vertices[v1]);
	face->m_Vertices.emplace_back(this->m_Vertices[v2]);
	face->m_Vertices.emplace_back(this->m_Vertices[v3]);

	face->m_Normal = glm::normalize(glm::cross(this->m_Vertices[v2]->m_Position - this->m_Vertices[v1]->m_Position, this->m_Vertices[v3]->m_Position - this->m_Vertices[v1]->m_Position));

	this->m_Faces.emplace_back(face);

	this->m_Vertices[v1]->m_Faces.emplace_back(face);
	this->m_Vertices[v2]->m_Faces.emplace_back(face);
	this->m_Vertices[v3]->m_Faces.emplace_back(face);

	this->AddEdge(v1, v2);
	this->AddEdge(v2, v3);
	this->AddEdge(v3, v1);
}

void SceneObject::AddFace(uint16_t v1, uint16_t v2, uint16_t v3, uint16_t v4) {
	Face* face = new Face;

	face->m_Index = m_Faces.size();
	face->m_Vertices.emplace_back(this->m_Vertices[v1]);
	face->m_Vertices.emplace_back(this->m_Vertices[v2]);
	face->m_Vertices.emplace_back(this->m_Vertices[v3]);
	face->m_Vertices.emplace_back(this->m_Vertices[v4]);

	face->m_Normal = glm::normalize(glm::cross(this->m_Vertices[v2]->m_Position - this->m_Vertices[v1]->m_Position, this->m_Vertices[v3]->m_Position - this->m_Vertices[v1]->m_Position));

	this->m_Faces.emplace_back(face);

	this->m_Vertices[v1]->m_Faces.emplace_back(face);
	this->m_Vertices[v2]->m_Faces.emplace_back(face);
	this->m_Vertices[v3]->m_Faces.emplace_back(face);
	this->m_Vertices[v4]->m_Faces.emplace_back(face);

	this->AddEdge(v1, v2);
	this->AddEdge(v2, v3);
	this->AddEdge(v3, v4);
	this->AddEdge(v4, v1);
}

void SceneObject::ComputeMesh() {
	// Compute faces
	for (unsigned int i = 0; i < this->m_Faces.size(); ++i) {
		Face* face = this->m_Faces[i];

		this->m_MeshIndices.emplace_back(face->m_Vertices[0]->m_Index);
		this->m_MeshIndices.emplace_back(face->m_Vertices[1]->m_Index);
		this->m_MeshIndices.emplace_back(face->m_Vertices[2]->m_Index);

		if (face->m_Vertices.size() == 4) {
			this->m_MeshIndices.emplace_back(face->m_Vertices[2]->m_Index);
			this->m_MeshIndices.emplace_back(face->m_Vertices[3]->m_Index);
			this->m_MeshIndices.emplace_back(face->m_Vertices[0]->m_Index);
		}
	}

	// Add vertices
	for (unsigned int i = 0; i < this->m_Vertices.size(); ++i) {
		vert v;
		v.position = this->m_Vertices[i]->m_Position;
		this->m_MeshVertices.emplace_back(v);
	}
}

bool SceneObject::FindEdgesFromVerticeAndFace(const Face* face, const Vertex* vertex, Edge** e1, Edge** e2) const {

	for (auto i = 0; i < vertex->m_Edges.size(); ++i) {
		Edge* edge = vertex->m_Edges[i];
		
		if (edge->m_F1 == face) {
			if (*e1 == NULL) {
				*e1 = edge;
			}
			else {
				*e2 = edge;
				return true;
			}
		}
		else if (edge->m_F2 == face) {
			if (*e1 == NULL) {
				*e1 = edge;
			}
			else {
				*e2 = edge;
				return true;
			}
		}
	}

	return false;
}

void SceneObject::CatmullClark(SceneObject& subdividedObject) const {
	uint16_t currentPointIndex = -1;
	std::vector<subVertex*> subVertices;
	std::vector<std::vector<uint16_t>> newFaces;

	// Compute face points
	for (auto i = 0; i < this->m_Faces.size(); ++i) {
		Face* face = this->m_Faces[i];
		glm::vec3 facePoint(0.f);

		for (auto j = 0; j < face->m_Vertices.size(); ++j) {
			facePoint += face->m_Vertices[j]->m_Position;
		}

		facePoint /= (float)face->m_Vertices.size();

		subVertex* subVert = new subVertex(++currentPointIndex, facePoint);
		subVertices.emplace_back(subVert);
		face->m_FacePoint = subVert;
	}

	// Compute edge points
	for (auto i = 0; i < this->m_Edges.size(); ++i) {
		Edge* edge = this->m_Edges[i];

		glm::vec3 edgePoint;
		if (edge->m_F2 == NULL) {
			edgePoint = (edge->m_V1->m_Position + edge->m_V2->m_Position + edge->m_F1->m_FacePoint->position + edge->m_F1->m_FacePoint->position) / 4.f;
		}
		else {
			edgePoint = (edge->m_V1->m_Position + edge->m_V2->m_Position + edge->m_F1->m_FacePoint->position + edge->m_F2->m_FacePoint->position) / 4.f;
		}
		

		subVertex* subVert = new subVertex(++currentPointIndex, edgePoint);
		subVertices.emplace_back(subVert);
		edge->m_EdgePoint = subVert;
	}

	// Compute vertex points
	for (auto i = 0; i < this->m_Vertices.size(); ++i) {
		Vertex* vertex = this->m_Vertices[i];
		glm::vec3 vertexPoint(0.f);

		float n = (float)vertex->m_Edges.size();
		glm::vec3 Q = glm::vec3(0.f);
		glm::vec3 R = glm::vec3(0.f);

		// Compute Q
		for (auto j = 0; j < vertex->m_Faces.size(); ++j) {
			Q += vertex->m_Faces[j]->m_FacePoint->position;
		}
		Q /= (float)vertex->m_Faces.size();

		// Compute R
		for (auto j = 0; j < vertex->m_Edges.size(); ++j) {
			glm::vec3 midpoint = (vertex->m_Edges[j]->m_V2->m_Position + vertex->m_Edges[j]->m_V1->m_Position) / 2.f;
			R += midpoint;
		}
		R /= n;

		// Compute vertex point
		vertexPoint = Q / n + 2.f * R / n + (n - 3.f) * vertex->m_Position / n;

		subVertex* subVert = new subVertex(++currentPointIndex, vertexPoint);
		subVertices.emplace_back(subVert);
		vertex->m_VertexPoint = subVert;
	}

	// Face connections
	for (auto i = 0; i < this->m_Faces.size(); ++i) {
		Face* face = this->m_Faces[i];
		
		for (auto j = 0; j < face->m_Vertices.size(); ++j) {
			Vertex* vertex = face->m_Vertices[j];
			std::vector<uint16_t> newFace;

			Edge* e1 = NULL;
			Edge* e2 = NULL;
			this->FindEdgesFromVerticeAndFace(face, vertex, &e1, &e2);

			if (e2 != NULL) {
				glm::vec3 normal = glm::normalize(glm::cross(e2->m_EdgePoint->position - face->m_FacePoint->position, vertex->m_VertexPoint->position - face->m_FacePoint->position));
				if (glm::dot(normal, face->m_Normal) >= 0.f) {
					newFace.emplace_back(face->m_FacePoint->index);
					newFace.emplace_back(e2->m_EdgePoint->index);
					newFace.emplace_back(vertex->m_VertexPoint->index);
					newFace.emplace_back(e1->m_EdgePoint->index);
				}
				else {
					newFace.emplace_back(e1->m_EdgePoint->index);
					newFace.emplace_back(vertex->m_VertexPoint->index);
					newFace.emplace_back(e2->m_EdgePoint->index);
					newFace.emplace_back(face->m_FacePoint->index);
				}

				newFaces.emplace_back(newFace);
			}
		}
	}
	
	for (auto i = 0; i < subVertices.size(); ++i) {
		subVertex* vert = subVertices[i];
		subdividedObject.AddVertex(subVertices[i]->position);
	}
	
	for (auto i = 0; i < newFaces.size(); ++i) {
		std::vector<uint16_t> face = newFaces[i];
		subdividedObject.AddFace(face[0], face[1], face[2], face[3]);
	}

	subdividedObject.ComputeMesh();
}

void SceneObject::BindObject() {
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->IBO);
	
	glGenVertexArrays(1, &this->VAO);

	int stride = sizeof(float) * 8;
	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, NULL);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(float)));
	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, stride * this->m_MeshVertices.size(), &this->m_MeshVertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * this->m_MeshIndices.size(), &this->m_MeshIndices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SceneObject::Draw(uint32_t program) {
	auto worldLocation = glGetUniformLocation(program, "u_worldMatrix");
	glUniformMatrix4fv(worldLocation, 1, GL_FALSE, glm::value_ptr(this->m_WorldMatrix));

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->IBO);
	glDrawElements(GL_TRIANGLES, this->m_MeshIndices.size(), GL_UNSIGNED_SHORT, NULL);
}

SceneObject::~SceneObject() {
	glDeleteBuffers(1, &this->VBO);
	glDeleteBuffers(1, &this->IBO);
	glDeleteVertexArrays(1, &this->VAO);
}